package io.jmix2mvp.petclinic;

public interface Authorities {
    String ADMIN = "ROLE_ADMIN";
    String USER = "ROLE_USER";
    String VETERINARIAN = "ROLE_VETERINARIAN";

}
