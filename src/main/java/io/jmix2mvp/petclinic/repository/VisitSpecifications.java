package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.Visit;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;

public class VisitSpecifications {
    public static Specification<Visit> byPetIdentificationNumberContains(String petIdentificationNumber) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get("pet").get("identificationNumber"), "%" + petIdentificationNumber + "%");
    }

    public static Specification<Visit> byOwnerFirstNameContains(String ownerFirstName) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get("pet").get("owner").get("firstName"), "%" + ownerFirstName + "%");
    }

    public static Specification<Visit> byOwnerLastNameContains(String ownerLastName) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get("pet").get("owner").get("lastName"), "%" + ownerLastName + "%");
    }

    public static Specification<Visit> byStartDateBefore(LocalDateTime visitStartBefore) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThan(root.get("visitStart"), visitStartBefore);
    }

    public static Specification<Visit> byStartDateAfter(LocalDateTime visitStartAfter) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get("visitStart"), visitStartAfter);
    }
}
