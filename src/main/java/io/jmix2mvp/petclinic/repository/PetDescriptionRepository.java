package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.PetDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PetDescriptionRepository extends JpaRepository<PetDescription, Long>, JpaSpecificationExecutor<PetDescription> {
}
