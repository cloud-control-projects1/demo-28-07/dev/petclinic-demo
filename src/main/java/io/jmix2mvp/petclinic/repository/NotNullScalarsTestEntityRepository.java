package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.NotNullScalarsTestEntity;
import io.jmix2mvp.petclinic.entity.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NotNullScalarsTestEntityRepository extends JpaRepository<NotNullScalarsTestEntity, Long>, JpaSpecificationExecutor<NotNullScalarsTestEntity> {
}