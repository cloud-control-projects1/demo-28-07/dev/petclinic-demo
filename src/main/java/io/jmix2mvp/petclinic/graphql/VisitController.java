package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.PetDTO;
import io.jmix2mvp.petclinic.dto.VisitDTO;
import io.jmix2mvp.petclinic.dto.VisitFilter;
import io.jmix2mvp.petclinic.dto.VisitInputDTO;
import io.jmix2mvp.petclinic.entity.Visit;
import io.jmix2mvp.petclinic.graphql.errors.PostponeTooLateException;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetRepository;
import io.jmix2mvp.petclinic.repository.VisitRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;
import static io.jmix2mvp.petclinic.repository.VisitSpecifications.*;

@Controller
public class VisitController {
    private final VisitRepository visitRepository;
    private final PetRepository petRepository;
    private final DTOMapper mapper;

    public VisitController(VisitRepository visitRepository, PetRepository petRepository, DTOMapper mapper) {
        this.visitRepository = visitRepository;
        this.petRepository = petRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "visit")
    @Transactional
    public VisitDTO findById(@GraphQLId @Argument Long id) {
        return visitRepository.findById(id)
                .map(mapper::visitToDTO)
                .orElse(null);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "visitListFull")
    @Transactional
    public List<VisitDTO> findAll() {
        return visitRepository.findAll().stream()
                .map(mapper::visitToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "visitList")
    @Transactional
    public List<VisitDTO> findAllByFilter(@Argument VisitFilter filter) {
        Specification<Visit> specification = Specification.where(null);
        if (filter != null) {
            if (filter.getOwnerFirstName() != null) {
                specification = specification.and(byOwnerFirstNameContains(filter.getOwnerFirstName()));
            }
            if (filter.getOwnerLastName() != null) {
                specification = specification.and(byOwnerLastNameContains(filter.getOwnerLastName()));
            }
            if (filter.getPetIdentificationNumber() != null) {
                specification = specification.and(byPetIdentificationNumberContains(filter.getPetIdentificationNumber()));
            }
            if (filter.getVisitStartBefore() != null) {
                specification = specification.and(byStartDateBefore(filter.getVisitStartBefore()));
            }
            if (filter.getVisitStartAfter() != null) {
                specification = specification.and(byStartDateAfter(filter.getVisitStartAfter()));
            }
        }

        return visitRepository.findAll(specification).stream()
                .map(mapper::visitToDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @SchemaMapping(value = "pet", typeName = "VisitDTO")
    @Transactional
    public PetDTO getPet(VisitDTO visitDTO) {
        return petRepository.findPetByVisit(visitDTO.getId())
                .map(mapper::petToDTO)
                .orElse(null);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updateVisit")
    @Transactional
    public VisitDTO update(@Argument VisitInputDTO input) {
        if (input.getId() != null) {
            if (!visitRepository.existsById(input.getId())) {
                throw new ResourceNotFoundException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }

        Visit entity = new Visit();
        mapper.visitDTOToEntity(input, entity);
        entity = visitRepository.save(entity);

        return mapper.visitToDTO(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deleteVisit")
    @Transactional
    public void delete(@GraphQLId @Argument Long id) {
        Visit entity = visitRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));

        visitRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "postponeVisit")
    @Transactional
    public VisitDTO postpone(@Argument @NonNull @GraphQLId Long visitId, @Argument @NonNull @Positive Integer days) {
        Visit entity = visitRepository.findById(visitId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", visitId)));

        LocalDateTime now = LocalDateTime.now();
        if (entity.getVisitStart().isBefore(now) || entity.getVisitEnd().isBefore(now)) {
            throw new PostponeTooLateException("Visit already completed, id=" + visitId);
        }
        entity.setVisitStart(entity.getVisitStart().plusDays(days));
        entity.setVisitEnd(entity.getVisitEnd().plusDays(days));
        Visit saved = visitRepository.save(entity);

        VisitDTO result = mapper.visitToDTO(saved);
        return result;
    }
}
