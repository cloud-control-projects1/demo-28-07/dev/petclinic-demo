package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.PetDiseaseDTO;
import io.jmix2mvp.petclinic.dto.PetDiseaseInputDTO;
import io.jmix2mvp.petclinic.entity.PetDisease;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetDiseaseRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@Controller
public class PetDiseaseController {
    private final PetDiseaseRepository crudRepository;
    private final DTOMapper mapper;

    public PetDiseaseController(PetDiseaseRepository crudRepository, DTOMapper mapper) {
        this.crudRepository = crudRepository;
        this.mapper = mapper;
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "deletePetDisease")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull Long id) {
        PetDisease entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petDiseaseList")
    @Transactional(readOnly = true)
    @NonNull
    public List<PetDiseaseDTO> findAll() {
        return crudRepository.findAll().stream()
                .map(mapper::petDiseaseToPetDiseaseDTO)
                .collect(Collectors.toList());
    }

    @Secured({ADMIN, VETERINARIAN})
    @QueryMapping(name = "petDisease")
    @Transactional(readOnly = true)
    @NonNull
    public PetDiseaseDTO findById(@GraphQLId @Argument @NonNull Long id) {
        return crudRepository.findById(id)
                .map(mapper::petDiseaseToPetDiseaseDTO)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @Secured({ADMIN, VETERINARIAN})
    @MutationMapping(name = "updatePetDisease")
    @Transactional
    @NonNull
    public PetDiseaseDTO update(@Argument @NonNull PetDiseaseInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        PetDisease entity = new PetDisease();
        mapper.updatePetDiseaseFromPetDiseaseInputDTO(input, entity);
        entity = crudRepository.save(entity);
        return mapper.petDiseaseToPetDiseaseDTO(entity);
    }
}