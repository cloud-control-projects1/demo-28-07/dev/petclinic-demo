package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import com.amplicode.core.graphql.paging.OffsetPageInput;
import com.amplicode.core.graphql.paging.ResultPage;
import io.jmix2mvp.petclinic.dto.ScalarsTestEntityFilter;
import io.jmix2mvp.petclinic.entity.ScalarsTestEntity;
import io.jmix2mvp.petclinic.repository.ScalarsTestEntityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class ScalarsTestEntityController {
    private final ScalarsTestEntityRepository crudRepository;

    public ScalarsTestEntityController(ScalarsTestEntityRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @MutationMapping(name = "deleteScalarsTestEntity")
    @Transactional
    public void delete(@GraphQLId @Argument Long id) {
        ScalarsTestEntity entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "scalarsTestEntityListFull")
    @Transactional
    public List<ScalarsTestEntity> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "scalarsTestEntity")
    @Transactional
    public ScalarsTestEntity findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateScalarsTestEntity")
    @Transactional
    public ScalarsTestEntity update(@Argument ScalarsTestEntity input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }


    @NonNull
    @QueryMapping(name = "scalarsTestEntityList")
    public ResultPage<ScalarsTestEntity> scalarsTestEntityListWithFilterSortPage(@Argument OffsetPageInput page, @Argument List<ScalarsTestEntityOrderByInput> sort, @Argument ScalarsTestEntityFilter filter) {
        Pageable pageable = Optional.ofNullable(page)
                .map(p -> PageRequest.of(p.getNumber(), p.getSize()).withSort(createSort(sort)))
                .orElseGet(() -> PageRequest.ofSize(20).withSort(createSort(sort)));
        Page<ScalarsTestEntity> result = crudRepository.findAll(createFilter(filter), pageable);
        return ResultPage.page(result.getContent(), result.getTotalElements());
    }

    protected Sort createSort(List<ScalarsTestEntityOrderByInput> sortInput) {
        if (sortInput == null || sortInput.isEmpty()) {
            return Sort.unsorted();
        }
        List<Sort.Order> orders = sortInput.stream()
                .map(item -> {
                    Sort.Direction direction;
                    if (item.getDirection() == SortDirection.ASC) {
                        direction = Sort.Direction.ASC;
                    } else {
                        direction = Sort.Direction.DESC;
                    }
                    switch (item.getProperty()) {
                        case INT_TEST:
                            return Sort.Order.by("intTest").with(direction);
                        case BYTE_TEST:
                            return Sort.Order.by("byteTest").with(direction);
                        case SHORT_TEST:
                            return Sort.Order.by("shortTest").with(direction);
                        case DOUBLE_TEST:
                            return Sort.Order.by("doubleTest").with(direction);
                        case FLOAT_TEST:
                            return Sort.Order.by("floatTest").with(direction);
                        case STRING:
                            return Sort.Order.by("string").with(direction);
                        case BOOL:
                            return Sort.Order.by("bool").with(direction);
                        case BIG_INT:
                            return Sort.Order.by("bigInt").with(direction);
                        case LONG_TEST:
                            return Sort.Order.by("longTest").with(direction);
                        case BIG_DECIMAL:
                            return Sort.Order.by("bigDecimal").with(direction);
                        case LOCAL_DATE:
                            return Sort.Order.by("localDate").with(direction);
                        case OFFSET_TIME:
                            return Sort.Order.by("offsetTime").with(direction);
                        case OFFSET_DATE_TIME:
                            return Sort.Order.by("offsetDateTime").with(direction);
                        case LOCAL_TIME:
                            return Sort.Order.by("localTime").with(direction);
                        case LOCAL_DATE_TIME:
                            return Sort.Order.by("localDateTime").with(direction);
                        case DATE_TEST:
                            return Sort.Order.by("dateTest").with(direction);
                        case URL:
                            return Sort.Order.by("url").with(direction);
                        case INT_PRIMITIVE:
                            return Sort.Order.by("intPrimitive").with(direction);
                        case BYTE_PRIMITIVE:
                            return Sort.Order.by("bytePrimitive").with(direction);
                        case SHORT_PRIMITIVE:
                            return Sort.Order.by("shortPrimitive").with(direction);
                        case DOUBLE_PRIMITIVE:
                            return Sort.Order.by("doublePrimitive").with(direction);
                        case FLOAT_PRIMITIVE:
                            return Sort.Order.by("floatPrimitive").with(direction);
                        case BOOL_PRIMITIVE:
                            return Sort.Order.by("boolPrimitive").with(direction);
                        case LONG_PRIMITIVE:
                            return Sort.Order.by("longPrimitive").with(direction);
                        default:
                            return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return Sort.by(orders);
    }

    static class ScalarsTestEntityOrderByInput {

        private ScalarsTestEntityOrderByProperty property;
        private SortDirection direction;

        public ScalarsTestEntityOrderByProperty getProperty() {
            return property;
        }

        public void setProperty(ScalarsTestEntityOrderByProperty property) {
            this.property = property;
        }

        public SortDirection getDirection() {
            return direction;
        }

        public void setDirection(SortDirection direction) {
            this.direction = direction;
        }
    }

    public enum SortDirection {ASC, DESC}

    public enum ScalarsTestEntityOrderByProperty {INT_TEST, BYTE_TEST, SHORT_TEST, DOUBLE_TEST, FLOAT_TEST, STRING, BOOL, BIG_INT, LONG_TEST, BIG_DECIMAL, LOCAL_DATE, OFFSET_TIME, OFFSET_DATE_TIME, LOCAL_TIME, LOCAL_DATE_TIME, DATE_TEST, URL, INT_PRIMITIVE, BYTE_PRIMITIVE, SHORT_PRIMITIVE, DOUBLE_PRIMITIVE, FLOAT_PRIMITIVE, BOOL_PRIMITIVE, LONG_PRIMITIVE}

    protected Specification<ScalarsTestEntity> createFilter(ScalarsTestEntityFilter filter) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter != null) {
                if (filter.getIntTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("intTest"), filter.getIntTest()));
                }
                if (filter.getByteTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("byteTest"), filter.getByteTest()));
                }
                if (filter.getShortTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("shortTest"), filter.getShortTest()));
                }
                if (filter.getDoubleTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("doubleTest"), filter.getDoubleTest()));
                }
                if (filter.getFloatTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("floatTest"), filter.getFloatTest()));
                }
                if (filter.getString() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("string"), filter.getString()));
                }
                if (filter.getBool() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("bool"), filter.getBool()));
                }
                if (filter.getBigInt() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("bigInt"), filter.getBigInt()));
                }
                if (filter.getLongTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("longTest"), filter.getLongTest()));
                }
                if (filter.getBigDecimal() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("bigDecimal"), filter.getBigDecimal()));
                }
                if (filter.getLocalDate() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("localDate"), filter.getLocalDate()));
                }
                if (filter.getOffsetTime() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("offsetTime"), filter.getOffsetTime()));
                }
                if (filter.getOffsetDateTime() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("offsetDateTime"), filter.getOffsetDateTime()));
                }
                if (filter.getLocalTime() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("localTime"), filter.getLocalTime()));
                }
                if (filter.getLocalDateTime() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("localDateTime"), filter.getLocalDateTime()));
                }
                if (filter.getDateTest() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("dateTest"), filter.getDateTest()));
                }
                if (filter.getUrl() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("url"), filter.getUrl()));
                }
// TODO need investigation about work filter with primitives
//                if (filter.getIntPrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("intPrimitive"), filter.getIntPrimitive()));
//                }
//                if (filter.getBytePrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("bytePrimitive"), filter.getBytePrimitive()));
//                }
//                if (filter.getShortPrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("shortPrimitive"), filter.getShortPrimitive()));
//                }
//                if (filter.getDoublePrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("doublePrimitive"), filter.getDoublePrimitive()));
//                }
//                if (filter.getFloatPrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("floatPrimitive"), filter.getFloatPrimitive()));
//                }
//                if (filter.getBoolPrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("boolPrimitive"), filter.getBoolPrimitive()));
//                }
//                if (filter.getLongPrimitive() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("longPrimitive"), filter.getLongPrimitive()));
//                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

}