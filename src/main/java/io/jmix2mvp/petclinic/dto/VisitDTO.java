package io.jmix2mvp.petclinic.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class VisitDTO extends BaseDTO {
    @NotNull
    private PetDTO pet;
    private LocalDateTime visitStart;
    private LocalDateTime visitEnd;
    private String description;

    public LocalDateTime getVisitStart() {
        return visitStart;
    }

    public void setVisitStart(LocalDateTime visitStart) {
        this.visitStart = visitStart;
    }

    public LocalDateTime getVisitEnd() {
        return visitEnd;
    }

    public void setVisitEnd(LocalDateTime visitEnd) {
        this.visitEnd = visitEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PetDTO getPet() {
        return pet;
    }

    public void setPet(PetDTO pet) {
        this.pet = pet;
    }
}
