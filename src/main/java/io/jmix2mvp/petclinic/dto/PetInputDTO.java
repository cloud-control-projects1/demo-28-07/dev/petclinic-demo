package io.jmix2mvp.petclinic.dto;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

public class PetInputDTO extends BaseDTO {
    @Pattern(regexp = "^[a-zA-Z0-9_]*$")
    @Size(max = 5)
    @NotBlank(message = "{Pet.identificationNumber.notBlank}")
    private String identificationNumber;
    @Past
    private LocalDate birthDate;
    @Min(0)
    private Integer weightInGrams;
    private PetTypeDTO type;
    private OwnerDTO owner;
    private List<TagDTO> tags;
    private List<PetDiseaseDTO> diseases;
    private PetDescriptionDTO description;

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public PetTypeDTO getType() {
        return type;
    }

    public void setType(PetTypeDTO type) {
        this.type = type;
    }

    public OwnerDTO getOwner() {
        return owner;
    }

    public void setOwner(OwnerDTO owner) {
        this.owner = owner;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public PetDescriptionDTO getDescription() {
        return description;
    }

    public void setDescription(PetDescriptionDTO description) {
        this.description = description;
    }

    public List<PetDiseaseDTO> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<PetDiseaseDTO> diseases) {
        this.diseases = diseases;
    }

    public Integer getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(Integer weightInGrams) {
        this.weightInGrams = weightInGrams;
    }
}
