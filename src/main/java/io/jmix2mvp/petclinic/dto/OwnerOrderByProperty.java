package io.jmix2mvp.petclinic.dto;

public enum OwnerOrderByProperty {
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    CITY("city");

    private final String propertyName;

    OwnerOrderByProperty(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
