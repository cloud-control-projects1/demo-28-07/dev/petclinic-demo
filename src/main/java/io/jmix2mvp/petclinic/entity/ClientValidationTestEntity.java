package io.jmix2mvp.petclinic.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;

@Entity
@Table(name = "client_validation_test_entity")
public class ClientValidationTestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "eula_accepted", nullable = false)
    @AssertTrue
    private boolean eulaAccepted;

    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "business_email")
    @Email(regexp = ".*@haulmont.com$")
    private String businessEmail;

    @Column(name = "required_without_directive", nullable = false)
    @NotNull
    private String requiredWithoutDirective;

    @Column(name = "url_without_directive")
    private URL urlWithoutDirective;

    @Column(name = "url_string")
    @org.hibernate.validator.constraints.URL
    private String urlString;

    @Column(name = "length")
    @Length(min = 3, max = 5)
    private String length;

    @Column(name = "size")
    @Size(min = 3, max = 5)
    private String size;

    @Column(name = "pattern")
    @Pattern(regexp = "^A.*")
    private String pattern;

    @Column(name = "null_field")
    @Null
    private Integer nullField;

    @Column(name = "not_empty")
    @NotEmpty
    private String notEmpty;

    @Column(name = "not_blank")
    @NotBlank
    private String notBlank;

    @Column(name = "positive_or_zero")
    @PositiveOrZero
    private Long positiveOrZero;

    @Column(name = "positive")
    @Positive
    private Integer positive;

    @Column(name = "negative_or_zero", nullable = false)
    @NegativeOrZero
    private int negativeOrZero;

    @Column(name = "negative", nullable = false)
    @Negative
    private double negative;

    @Column(name = "quantity")
    @Max(value = 10000)
    @Min(value = 500)
    private Long quantity;

    @Temporal(TemporalType.DATE)
    @Column(name = "past_or_present")
    @PastOrPresent
    private Date pastOrPresent;

    @Column(name = "past")
    @Past
    private OffsetDateTime past;

    @Column(name = "future_or_present")
    @FutureOrPresent
    private LocalDate futureOrPresent;

    @Temporal(TemporalType.DATE)
    @Column(name = "future")
    @Future
    private Date future;

    @Column(name = "price", precision = 19, scale = 2)
    @DecimalMax(value = "199.99")
    @DecimalMin(value = "150", inclusive = false)
    @Digits(integer = 3, fraction = 2)
    private BigDecimal price;

    @Column(name = "trial_expired")
    @AssertFalse
    private Boolean trialExpired;

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getRequiredWithoutDirective() {
        return requiredWithoutDirective;
    }

    public void setRequiredWithoutDirective(String requiredWithoutDirective) {
        this.requiredWithoutDirective = requiredWithoutDirective;
    }

    public URL getUrlWithoutDirective() {
        return urlWithoutDirective;
    }

    public void setUrlWithoutDirective(URL urlWithoutDirective) {
        this.urlWithoutDirective = urlWithoutDirective;
    }

    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Integer getNullField() {
        return nullField;
    }

    public void setNullField(Integer nullField) {
        this.nullField = nullField;
    }

    public String getNotEmpty() {
        return notEmpty;
    }

    public void setNotEmpty(String notEmpty) {
        this.notEmpty = notEmpty;
    }

    public String getNotBlank() {
        return notBlank;
    }

    public void setNotBlank(String notBlank) {
        this.notBlank = notBlank;
    }

    public Long getPositiveOrZero() {
        return positiveOrZero;
    }

    public void setPositiveOrZero(Long positiveOrZero) {
        this.positiveOrZero = positiveOrZero;
    }

    public Integer getPositive() {
        return positive;
    }

    public void setPositive(Integer positive) {
        this.positive = positive;
    }

    public int getNegativeOrZero() {
        return negativeOrZero;
    }

    public void setNegativeOrZero(int negativeOrZero) {
        this.negativeOrZero = negativeOrZero;
    }

    public double getNegative() {
        return negative;
    }

    public void setNegative(double negative) {
        this.negative = negative;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getPastOrPresent() {
        return pastOrPresent;
    }

    public void setPastOrPresent(Date pastOrPresent) {
        this.pastOrPresent = pastOrPresent;
    }

    public OffsetDateTime getPast() {
        return past;
    }

    public void setPast(OffsetDateTime past) {
        this.past = past;
    }

    public LocalDate getFutureOrPresent() {
        return futureOrPresent;
    }

    public void setFutureOrPresent(LocalDate futureOrPresent) {
        this.futureOrPresent = futureOrPresent;
    }

    public Date getFuture() {
        return future;
    }

    public void setFuture(Date future) {
        this.future = future;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getTrialExpired() {
        return trialExpired;
    }

    public void setTrialExpired(Boolean trialExpired) {
        this.trialExpired = trialExpired;
    }

    public boolean getEulaAccepted() {
        return eulaAccepted;
    }

    public void setEulaAccepted(boolean eulaAccepted) {
        this.eulaAccepted = eulaAccepted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
