package io.jmix2mvp.petclinic.entity.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PetTestConstraintValidator1.class)
@Documented
public @interface PetTestConstraint1 {
    String message() default "{Pet.PetTestConstraint1}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}