package io.jmix2mvp.petclinic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.*;
import java.util.Date;

@Entity
@Table(name = "not_null_scalars_test_entity")
public class NotNullScalarsTestEntity extends BaseEntity {

    @NotNull
    @Column(name = "string_not_null", nullable = false)
    private String stringNotNull;

    @NotNull
    @Column(name = "big_int_not_null", nullable = false)
    private BigInteger bigIntNotNull;

    @NotNull
    @Column(name = "big_decimal_not_null", nullable = false, precision = 19, scale = 2)
    private BigDecimal bigDecimalNotNull;

    @NotNull
    @Column(name = "local_date_not_null", nullable = false)
    private LocalDate localDateNotNull;

    @NotNull
    @Column(name = "offset_time_not_null", nullable = false)
    private OffsetTime offsetTimeNotNull;

    @NotNull
    @Column(name = "offset_date_time_not_null", nullable = false)
    private OffsetDateTime offsetDateTimeNotNull;

    @NotNull
    @Column(name = "local_time_not_null", nullable = false)
    private LocalTime localTimeNotNull;

    @NotNull
    @Column(name = "local_date_time_not_null", nullable = false)
    private LocalDateTime localDateTimeNotNull;

    @NotNull
    @Column(name = "date_test_not_null", nullable = false)
    private Date dateTestNotNull;

    @NotNull
    @Column(name = "url_not_null", nullable = false)
    private URL urlNotNull;


    public String getStringNotNull() {
        return stringNotNull;
    }

    public void setStringNotNull(String stringNotNull) {
        this.stringNotNull = stringNotNull;
    }

    public BigInteger getBigIntNotNull() {
        return bigIntNotNull;
    }

    public void setBigIntNotNull(BigInteger bigIntNotNull) {
        this.bigIntNotNull = bigIntNotNull;
    }

    public BigDecimal getBigDecimalNotNull() {
        return bigDecimalNotNull;
    }

    public void setBigDecimalNotNull(BigDecimal bigDecimalNotNull) {
        this.bigDecimalNotNull = bigDecimalNotNull;
    }

    public LocalDate getLocalDateNotNull() {
        return localDateNotNull;
    }

    public void setLocalDateNotNull(LocalDate localDateNotNull) {
        this.localDateNotNull = localDateNotNull;
    }

    public OffsetTime getOffsetTimeNotNull() {
        return offsetTimeNotNull;
    }

    public void setOffsetTimeNotNull(OffsetTime offsetTimeNotNull) {
        this.offsetTimeNotNull = offsetTimeNotNull;
    }

    public OffsetDateTime getOffsetDateTimeNotNull() {
        return offsetDateTimeNotNull;
    }

    public void setOffsetDateTimeNotNull(OffsetDateTime offsetDateTimeNotNull) {
        this.offsetDateTimeNotNull = offsetDateTimeNotNull;
    }

    public LocalTime getLocalTimeNotNull() {
        return localTimeNotNull;
    }

    public void setLocalTimeNotNull(LocalTime localTimeNotNull) {
        this.localTimeNotNull = localTimeNotNull;
    }

    public LocalDateTime getLocalDateTimeNotNull() {
        return localDateTimeNotNull;
    }

    public void setLocalDateTimeNotNull(LocalDateTime localDateTimeNotNull) {
        this.localDateTimeNotNull = localDateTimeNotNull;
    }

    public Date getDateTestNotNull() {
        return dateTestNotNull;
    }

    public void setDateTestNotNull(Date dateTestNotNull) {
        this.dateTestNotNull = dateTestNotNull;
    }

    public URL getUrlNotNull() {
        return urlNotNull;
    }

    public void setUrlNotNull(URL urlNotNull) {
        this.urlNotNull = urlNotNull;
    }

}