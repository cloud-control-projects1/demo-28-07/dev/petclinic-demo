package io.jmix2mvp.petclinic.config;

import com.amplicode.core.auth.AuthenticationInfoProvider;
import com.amplicode.core.auth.UserDetailsAuthenticationInfoProvider;
import com.amplicode.core.security.Authorities;
import com.amplicode.core.security.UnauthorizedStatusAuthenticationEntryPoint;
import com.amplicode.core.security.formlogin.FormLoginAuthenticationFailureHandler;
import com.amplicode.core.security.formlogin.FormLoginAuthenticationSuccessHandler;
import com.amplicode.core.security.formlogin.FormLoginLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration {

    private final String adminUsername;
    private final String adminPassword;
    private final String userUsername;
    private final String userPassword;

    public SecurityConfiguration(@Value("${app.security.in-memory.admin.username}") String adminUsername,
                                 @Value("${app.security.in-memory.admin.password}") String adminPassword,
                                 @Value("${app.security.in-memory.user.username}") String userUsername,
                                 @Value("${app.security.in-memory.user.password}") String userPassword) {
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
        this.userUsername = userUsername;
        this.userPassword = userPassword;
    }

    @Bean
    public FormLoginAuthenticationSuccessHandler formLoginAuthenticationSuccessHandler() {
        return new FormLoginAuthenticationSuccessHandler();
    }

    @Bean
    public FormLoginAuthenticationFailureHandler formLoginAuthenticationFailureHandler() {
        return new FormLoginAuthenticationFailureHandler();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new FormLoginLogoutSuccessHandler();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new UnauthorizedStatusAuthenticationEntryPoint();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //Form Login
        http.formLogin(formLogin -> formLogin
                .successHandler(formLoginAuthenticationSuccessHandler())
                .failureHandler(formLoginAuthenticationFailureHandler())
                .permitAll());
        //Exception handling
        http.exceptionHandling(exceptionHandling -> exceptionHandling
                .authenticationEntryPoint(authenticationEntryPoint()));
        //Logout
        http.logout(logout -> logout
                .logoutSuccessHandler(logoutSuccessHandler()));
        //Authorize
        http.authorizeRequests(authorization -> authorization
                .antMatchers("/graphql").permitAll()
                .antMatchers("/graphql/**").permitAll());
        //CORS
        http.cors(withDefaults());
        //CSRF
        http.csrf(AbstractHttpConfigurer::disable);
        return http.build();
    }

    @Bean
    public UserDetailsService inMemoryUsers() {
        User.UserBuilder users = User.builder();
        InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
        userDetailsManager.createUser(users.username(adminUsername)
                .password(adminPassword)
                .authorities(io.jmix2mvp.petclinic.Authorities.ADMIN, Authorities.FULL_ACCESS)
                .build());
        userDetailsManager.createUser(users.username(userUsername)
                .password(userPassword)
                .roles("VETERINARIAN")
                .build());
        return userDetailsManager;
    }

    @Bean
    public AuthenticationInfoProvider authenticationInfoProvider() {
        return new UserDetailsAuthenticationInfoProvider();
    }
}
