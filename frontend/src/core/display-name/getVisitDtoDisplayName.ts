import { VisitDto } from "../../gql/graphql";

export function getVisitDtoDisplayName(
  entityInstance?: VisitDto | null
): string {
  if (entityInstance == null) {
    return "";
  }
  if (entityInstance.description != null) {
    return String(entityInstance.description);
  }
  if (entityInstance.id != null) {
    return String(entityInstance.id);
  }
  return JSON.stringify(entityInstance);
}
