import { useMemo, ReactNode } from "react";
import { useQuery, useMutation } from "@apollo/client";
import { ApolloError } from "@apollo/client/errors";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Button, Modal, message, Card, Empty, Space, Spin } from "antd";
import {
  DeleteOutlined,
  LoadingOutlined,
  EditOutlined,
  PlusOutlined
} from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { FormattedMessage, useIntl } from "react-intl";
import { gql } from "@amplicode/gql";
import { ValueWithLabel } from "../../../core/crud/ValueWithLabel";
import { useDeleteItem } from "../../../core/crud/useDeleteItem";
import { GraphQLError } from "graphql/error/GraphQLError";
import { FetchResult } from "@apollo/client/link/core";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { getClientValidationTestEntityDisplayName } from "../../../core/display-name/getClientValidationTestEntityDisplayName";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";

const REFETCH_QUERIES = ["Get_Client_Validation_Test_Entity_List"];

const CLIENT_VALIDATION_TEST_ENTITY_LIST = gql(`
  query Get_Client_Validation_Test_Entity_List {
    clientValidationTestEntityList {
      id
      businessEmail 
      email 
      eulaAccepted 
      future 
      futureOrPresent 
      id 
      length 
      negative 
      negativeOrZero 
      notBlank 
      notEmpty 
      nullField 
      past 
      pastOrPresent 
      pattern 
      positive 
      positiveOrZero 
      price 
      quantity 
      requiredWithoutDirective 
      size 
      trialExpired 
      urlString 
      urlWithoutDirective 
    }
  }
`);

const DELETE_CLIENT_VALIDATION_TEST_ENTITY = gql(`
  mutation Delete_Client_Validation_Test_Entity($id: ID!) {
    deleteClientValidationTestEntity(id: $id)
  }
`);

export function ClientValidationTestEntityCards() {
  const intl = useIntl();
  useBreadcrumbItem(
    intl.formatMessage({ id: "screen.ClientValidationTestEntityCards" })
  );

  // Load the items from server
  const { loading, error, data } = useQuery(CLIENT_VALIDATION_TEST_ENTITY_LIST);

  const items = useMemo(
    () => deserialize(data?.clientValidationTestEntityList),
    [data?.clientValidationTestEntityList]
  );

  return (
    <div className="narrow-layout">
      <Space direction="vertical" className="card-space">
        <ButtonPanel />
        <Cards items={items} loading={loading} error={error} />
      </Space>
    </div>
  );
}

/**
 * Button panel above the cards
 */
function ButtonPanel() {
  const intl = useIntl();
  const navigate = useNavigate();

  return (
    <Space>
      <Button
        htmlType="button"
        key="create"
        title={intl.formatMessage({ id: "common.create" })}
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => navigate("new")}
      >
        <span>
          <FormattedMessage id="common.create" />
        </span>
      </Button>
    </Space>
  );
}

interface ItemCardsListProps {
  items?: ItemListType;
  loading?: boolean;
  error?: ApolloError;
}

/**
 * Collection of cards, each card representing an item
 */
function Cards({ items, loading, error }: ItemCardsListProps) {
  if (loading) {
    return <Spin />;
  }

  if (error) {
    return <RequestFailedError />;
  }

  if (items == null || items.length === 0) {
    return <Empty />;
  }

  return (
    <Space direction="vertical" className="card-space">
      {items.map(item => (
        <ItemCard item={item} key={item?.id} />
      ))}
    </Space>
  );
}

function ItemCard({ item }: { item: ItemType }) {
  // Get the action buttons that will be displayed in the card
  const cardActions: ReactNode[] = useCardActions(item);

  if (item == null) {
    return null;
  }

  return (
    <Card
      key={item.id}
      title={getClientValidationTestEntityDisplayName(item)}
      actions={cardActions}
      className="narrow-layout"
    >
      <ValueWithLabel
        key="businessEmail"
        label="Business Email"
        value={item.businessEmail ?? undefined}
      />
      <ValueWithLabel
        key="email"
        label="Email"
        value={item.email ?? undefined}
      />
      <ValueWithLabel
        key="eulaAccepted"
        label="Eula Accepted"
        value={item.eulaAccepted ?? undefined}
      />
      <ValueWithLabel
        key="future"
        label="Future"
        value={item.future?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="futureOrPresent"
        label="Future Or Present"
        value={item.futureOrPresent?.format("LL") ?? undefined}
      />
      <ValueWithLabel
        key="length"
        label="Length"
        value={item.length ?? undefined}
      />
      <ValueWithLabel
        key="negative"
        label="Negative"
        value={item.negative ?? undefined}
      />
      <ValueWithLabel
        key="negativeOrZero"
        label="Negative Or Zero"
        value={item.negativeOrZero ?? undefined}
      />
      <ValueWithLabel
        key="notBlank"
        label="Not Blank"
        value={item.notBlank ?? undefined}
      />
      <ValueWithLabel
        key="notEmpty"
        label="Not Empty"
        value={item.notEmpty ?? undefined}
      />
      <ValueWithLabel
        key="nullField"
        label="Null Field"
        value={item.nullField ?? undefined}
      />
      <ValueWithLabel
        key="past"
        label="Past"
        value={item.past?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="pastOrPresent"
        label="Past Or Present"
        value={item.pastOrPresent?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="pattern"
        label="Pattern"
        value={item.pattern ?? undefined}
      />
      <ValueWithLabel
        key="positive"
        label="Positive"
        value={item.positive ?? undefined}
      />
      <ValueWithLabel
        key="positiveOrZero"
        label="Positive Or Zero"
        value={item.positiveOrZero ?? undefined}
      />
      <ValueWithLabel
        key="price"
        label="Price"
        value={item.price ?? undefined}
      />
      <ValueWithLabel
        key="quantity"
        label="Quantity"
        value={item.quantity ?? undefined}
      />
      <ValueWithLabel
        key="requiredWithoutDirective"
        label="Required Without Directive"
        value={item.requiredWithoutDirective ?? undefined}
      />
      <ValueWithLabel key="size" label="Size" value={item.size ?? undefined} />
      <ValueWithLabel
        key="trialExpired"
        label="Trial Expired"
        value={item.trialExpired ?? undefined}
      />
      <ValueWithLabel
        key="urlString"
        label="Url String"
        value={item.urlString ?? undefined}
      />
      <ValueWithLabel
        key="urlWithoutDirective"
        label="Url Without Directive"
        value={item.urlWithoutDirective ?? undefined}
        isUrl={true}
      />
    </Card>
  );
}

/**
 * Returns action buttons that will be displayed inside the card.
 */
function useCardActions(item: ItemType): ReactNode[] {
  const intl = useIntl();
  const navigate = useNavigate();
  const { showDeleteConfirm, deleting } = useDeleteConfirm(item?.id);

  return [
    <EditOutlined
      key="edit"
      title={intl.formatMessage({ id: "common.edit" })}
      onClick={() => {
        if (item?.id != null) {
          navigate(item.id);
        }
      }}
    />,
    deleting ? (
      <LoadingOutlined />
    ) : (
      <DeleteOutlined
        key="delete"
        title={intl.formatMessage({ id: "common.remove" })}
        onClick={showDeleteConfirm}
      />
    )
  ];
}

/**
 * Returns a confirmation dialog and invokes delete mutation upon confirmation
 * @param id id of the entity instance that should be deleted
 */
function useDeleteConfirm(id: string | null | undefined) {
  const intl = useIntl();

  const [runDeleteMutation, { loading }] = useMutation(
    DELETE_CLIENT_VALIDATION_TEST_ENTITY
  );
  const deleteItem = useDeleteItem(id, runDeleteMutation, REFETCH_QUERIES);

  // Callback that deletes the item
  function handleDeleteItem() {
    deleteItem()
      .then(({ errors }: FetchResult) => {
        if (errors == null || errors.length === 0) {
          return handleDeleteSuccess();
        }
        return handleDeleteGraphQLError(errors);
      })
      .catch(handleDeleteNetworkError);
  }

  // Function that is executed when mutation is successful
  function handleDeleteSuccess() {
    return message.success(
      intl.formatMessage({ id: "EntityDetailsScreen.deletedSuccessfully" })
    );
  }

  // Function that is executed when mutation results in a GraphQL error
  function handleDeleteGraphQLError(
    errors: ReadonlyArray<GraphQLError> | undefined
  ) {
    console.error(errors);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  // Function that is executed when mutation results in a network error (such as 4xx or 5xx)
  function handleDeleteNetworkError(error: Error | ApolloError) {
    console.error(error);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  return {
    showDeleteConfirm: () =>
      Modal.confirm({
        content: intl.formatMessage({
          id: "EntityListScreen.deleteConfirmation"
        }),
        okText: intl.formatMessage({ id: "common.ok" }),
        cancelText: intl.formatMessage({ id: "common.cancel" }),
        onOk: handleDeleteItem
      }),
    deleting: loading
  };
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof CLIENT_VALIDATION_TEST_ENTITY_LIST>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["clientValidationTestEntityList"];
/**
 * Type of a single item
 */
type ItemType = Exclude<ItemListType, null | undefined>[0];
