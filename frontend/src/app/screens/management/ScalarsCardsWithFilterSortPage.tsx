import { useMemo, ReactNode, useState, useCallback, useEffect } from "react";
import { useQuery, useMutation } from "@apollo/client";
import { ApolloError } from "@apollo/client/errors";
import { ResultOf, VariablesOf } from "@graphql-typed-document-node/core";
import {
  Button,
  Modal,
  message,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
  Checkbox,
  Card,
  Empty,
  Space,
  Spin,
  Badge,
  Pagination,
  Select
} from "antd";
import { useForm } from "antd/lib/form/Form";
import { serializeVariables } from "../../../core/transform/model/serializeVariables";
import { DatePicker, TimePicker } from "@amplicode/react";
import {
  DeleteOutlined,
  LoadingOutlined,
  EditOutlined,
  PlusOutlined,
  CloseCircleOutlined,
  UpOutlined,
  DownOutlined,
  ArrowUpOutlined,
  ArrowDownOutlined
} from "@ant-design/icons";
import { useNavigate, useSearchParams } from "react-router-dom";
import { FormattedMessage, useIntl } from "react-intl";
import { gql } from "@amplicode/gql";
import { ValueWithLabel } from "../../../core/crud/ValueWithLabel";
import { useDeleteItem } from "../../../core/crud/useDeleteItem";
import { GraphQLError } from "graphql/error/GraphQLError";
import { FetchResult } from "@apollo/client/link/core";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { getScalarsTestEntityDisplayName } from "../../../core/display-name/getScalarsTestEntityDisplayName";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";
import { NamePath } from "antd/lib/form/interface";
import {
  SortDirection,
  ScalarsTestEntityOrderByProperty
} from "../../../gql/graphql";
import { DefaultOptionType } from "antd/lib/select";

const REFETCH_QUERIES = ["Get_Scalars_Test_Entity_List_With_Filter_Page_Sort"];

const SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE = gql(`
  query Get_Scalars_Test_Entity_List_With_Filter_Page_Sort($filter: ScalarsTestEntityFilterInput, $page: OffsetPageInput, $sort: [ScalarsTestEntityOrderByInput]) {
  scalarsTestEntityList(filter: $filter, page: $page, sort: $sort) {
    content {
      id
      intTest
      intPrimitive
      byteTest
      bytePrimitive
      shortTest
      shortPrimitive
      doubleTest
      doublePrimitive
      floatTest
      floatPrimitive
      string
      bool
      boolPrimitive
      bigInt
      longTest
      longPrimitive
      bigDecimal
      localDate
      localDateTime
      localTime
      offsetDateTime
      offsetTime
      dateTest
      url
    }
    totalElements
  }
}
`);

const DELETE_SCALARS_TEST_ENTITY = gql(`
  mutation Delete_Scalars($id: ID!) {
    deleteScalarsTestEntity(id: $id)
  }
`);

const DEFAULT_PAGE_SIZE = 10;

export function ScalarsCardsWithFilterSortPage() {
  const intl = useIntl();
  useBreadcrumbItem(
    intl.formatMessage({ id: "screen.ScalarsCardsWithFilterSortPage" })
  );

  const [searchParams, setSearchParams] = useSearchParams();
  // Selection state is initialized to URL search params
  const [selectionState, setSelectionState] = useState<QueryVariablesType>(
    searchParamsToState(searchParams)
  );

  const [initialFilterValues] = useState<QueryVariablesType>(
    extractFilterParams(selectionState)
  );

  useEffect(() => {
    // Whenever selection state is changed, update URL search params accordingly
    setSearchParams(stateToSearchParams(selectionState));
  }, [selectionState, setSearchParams]);

  // Load the items from server. Will be reloaded reactively if one of variable changes
  const {
    loading,
    error,
    data
  } = useQuery(SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE, {
    variables: selectionState
  });

  const items = useMemo(
    () => deserialize(data?.scalarsTestEntityList?.content),
    [data?.scalarsTestEntityList?.content]
  );

  const applyPagination = useCallback((current: number, pageSize: number) => {
    setSelectionState(prevState => ({
      ...prevState,
      page: {
        number: current - 1,
        size: pageSize
      }
    }));
  }, []);

  const applySort = useCallback(
    (newSortValue: QueryVariablesType["sort"] | undefined) => {
      setSelectionState(prevState => {
        const newState = { ...prevState };
        if (newSortValue != null) {
          newState.sort = newSortValue;
        } else {
          delete newState.sort;
        }
        return newState;
      });
    },
    []
  );

  const applyFilters = useCallback((filters: QueryVariablesType) => {
    setSelectionState(prevState => {
      const newFilters = serializeVariables(
        SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE,
        filters
      );
      return {
        ...prevState,
        page: {
          number: 0,
          size: prevState.page?.size ?? DEFAULT_PAGE_SIZE
        },
        ...newFilters
      };
    });
  }, []);

  return (
    <div className="narrow-layout">
      <Space direction="vertical" className="card-space">
        <Card>
          <Filters
            onApplyFilters={applyFilters}
            initialFilterValues={initialFilterValues}
          />
        </Card>
        <ButtonPanel onApplySort={applySort} sortValue={selectionState.sort} />
        <Cards items={items} loading={loading} error={error} />
        <Pagination
          current={
            selectionState.page?.number != null
              ? selectionState.page?.number + 1
              : undefined
          }
          pageSize={selectionState.page?.size}
          onChange={applyPagination}
          showSizeChanger
          total={data?.scalarsTestEntityList?.totalElements}
        />
      </Space>
    </div>
  );
}

const sortBySelectorOptions: DefaultOptionType[] = [
  {
    label: (
      <>
        Big Decimal (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.BigDecimal
    })
  },
  {
    label: (
      <>
        Big Decimal (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.BigDecimal
    })
  },
  {
    label: (
      <>
        Big Int (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.BigInt
    })
  },
  {
    label: (
      <>
        Big Int (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.BigInt
    })
  },
  {
    label: (
      <>
        Bool (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.Bool
    })
  },
  {
    label: (
      <>
        Bool (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.Bool
    })
  },
  {
    label: (
      <>
        Bool Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.BoolPrimitive
    })
  },
  {
    label: (
      <>
        Bool Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.BoolPrimitive
    })
  },
  {
    label: (
      <>
        Byte Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.BytePrimitive
    })
  },
  {
    label: (
      <>
        Byte Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.BytePrimitive
    })
  },
  {
    label: (
      <>
        Byte Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.ByteTest
    })
  },
  {
    label: (
      <>
        Byte Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.ByteTest
    })
  },
  {
    label: (
      <>
        Date Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.DateTest
    })
  },
  {
    label: (
      <>
        Date Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.DateTest
    })
  },
  {
    label: (
      <>
        Double Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.DoublePrimitive
    })
  },
  {
    label: (
      <>
        Double Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.DoublePrimitive
    })
  },
  {
    label: (
      <>
        Double Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.DoubleTest
    })
  },
  {
    label: (
      <>
        Double Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.DoubleTest
    })
  },
  {
    label: (
      <>
        Float Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.FloatPrimitive
    })
  },
  {
    label: (
      <>
        Float Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.FloatPrimitive
    })
  },
  {
    label: (
      <>
        Float Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.FloatTest
    })
  },
  {
    label: (
      <>
        Float Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.FloatTest
    })
  },
  {
    label: (
      <>
        Int Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.IntPrimitive
    })
  },
  {
    label: (
      <>
        Int Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.IntPrimitive
    })
  },
  {
    label: (
      <>
        Int Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.IntTest
    })
  },
  {
    label: (
      <>
        Int Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.IntTest
    })
  },
  {
    label: (
      <>
        Local Date (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.LocalDate
    })
  },
  {
    label: (
      <>
        Local Date (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.LocalDate
    })
  },
  {
    label: (
      <>
        Local Date Time (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.LocalDateTime
    })
  },
  {
    label: (
      <>
        Local Date Time (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.LocalDateTime
    })
  },
  {
    label: (
      <>
        Local Time (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.LocalTime
    })
  },
  {
    label: (
      <>
        Local Time (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.LocalTime
    })
  },
  {
    label: (
      <>
        Long Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.LongPrimitive
    })
  },
  {
    label: (
      <>
        Long Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.LongPrimitive
    })
  },
  {
    label: (
      <>
        Long Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.LongTest
    })
  },
  {
    label: (
      <>
        Long Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.LongTest
    })
  },
  {
    label: (
      <>
        Offset Date Time (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.OffsetDateTime
    })
  },
  {
    label: (
      <>
        Offset Date Time (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.OffsetDateTime
    })
  },
  {
    label: (
      <>
        Offset Time (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.OffsetTime
    })
  },
  {
    label: (
      <>
        Offset Time (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.OffsetTime
    })
  },
  {
    label: (
      <>
        Short Primitive (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.ShortPrimitive
    })
  },
  {
    label: (
      <>
        Short Primitive (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.ShortPrimitive
    })
  },
  {
    label: (
      <>
        Short Test (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.ShortTest
    })
  },
  {
    label: (
      <>
        Short Test (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.ShortTest
    })
  },
  {
    label: (
      <>
        String (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.String
    })
  },
  {
    label: (
      <>
        String (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.String
    })
  },
  {
    label: (
      <>
        Url (<ArrowDownOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Desc,
      property: ScalarsTestEntityOrderByProperty.Url
    })
  },
  {
    label: (
      <>
        Url (<ArrowUpOutlined />)
      </>
    ),
    value: JSON.stringify({
      direction: SortDirection.Asc,
      property: ScalarsTestEntityOrderByProperty.Url
    })
  }
];

interface ButtonPanelProps {
  onApplySort: (sort: QueryVariablesType["sort"]) => void;
  sortValue?: QueryVariablesType["sort"];
}
/**
 * Button panel above the cards
 */
function ButtonPanel({ onApplySort, sortValue }: ButtonPanelProps) {
  const intl = useIntl();
  const navigate = useNavigate();

  return (
    <Row justify="space-between" gutter={[16, 8]}>
      <Col>
        <Space>
          <Button
            htmlType="button"
            key="create"
            title={intl.formatMessage({ id: "common.create" })}
            type="primary"
            icon={<PlusOutlined />}
            onClick={() => navigate("new")}
          >
            <span>
              <FormattedMessage id="common.create" />
            </span>
          </Button>
        </Space>
      </Col>
      <Col>
        <Select
          value={JSON.stringify(sortValue)}
          className="sort-by-select-width"
          allowClear
          placeholder={intl.formatMessage({ id: "sort.sortBy" })}
          onChange={sortBy => onApplySort(sortBy && JSON.parse(sortBy))}
          options={sortBySelectorOptions}
        />
      </Col>
    </Row>
  );
}

const couldBeHiddenFilters: NamePath[] = [
  ["filter", "floatTest"],

  ["filter", "string"],

  ["filter", "bool"],

  ["filter", "bigInt"],

  ["filter", "longTest"],

  ["filter", "bigDecimal"],

  ["filter", "localDate"],

  ["filter", "localDateTime"],

  ["filter", "localTime"],

  ["filter", "offsetDateTime"],

  ["filter", "offsetTime"],

  ["filter", "dateTest"],

  ["filter", "url"]
];

interface FiltersProps {
  onApplyFilters: (queryVariables: QueryVariablesType) => void;
  initialFilterValues: QueryVariablesType;
}

function Filters({ onApplyFilters, initialFilterValues }: FiltersProps) {
  const [form] = useForm();

  const [showAll, setShowAll] = useState(false);

  const [countHiddenTouchedFilters, setCountHiddenTouchedFilters] = useState(0);
  const [countHiddenInvalideFilters, setCountHiddenInvalideFilters] = useState(
    0
  );

  useEffect(() => {
    form.setFieldsValue(initialFilterValues);
  }, [form, initialFilterValues]);

  const onResetFilters = useCallback(async () => {
    await form.resetFields();
    const filters = await form.validateFields();
    onApplyFilters(filters);
  }, [form, onApplyFilters]);

  return (
    <Form form={form} layout="vertical" onFinish={onApplyFilters}>
      <Form.Item shouldUpdate>
        {() => {
          const newCountHiddenTouchedFilters = showAll
            ? 0
            : couldBeHiddenFilters.filter(filterName =>
                form.isFieldTouched(filterName)
              ).length;
          if (newCountHiddenTouchedFilters !== countHiddenTouchedFilters) {
            setCountHiddenTouchedFilters(newCountHiddenTouchedFilters);
          }

          const newCountHiddenInvalideFilters = showAll
            ? 0
            : couldBeHiddenFilters.filter(
                filterName => form.getFieldError(filterName).length > 0
              ).length;
          if (newCountHiddenInvalideFilters !== countHiddenInvalideFilters) {
            setCountHiddenInvalideFilters(newCountHiddenInvalideFilters);
          }

          return (
            <Row gutter={16}>
              <Col span={6}>
                <Form.Item name={["filter", "intTest"]} label="Int Test">
                  <InputNumber
                    type="number"
                    precision={0}
                    max={2147483647}
                    min={-2147483648}
                  />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item name={["filter", "byteTest"]} label="Byte Test">
                  <InputNumber
                    type="number"
                    precision={0}
                    max={2147483647}
                    min={-2147483648}
                  />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item name={["filter", "shortTest"]} label="Short Test">
                  <InputNumber
                    type="number"
                    precision={0}
                    max={2147483647}
                    min={-2147483648}
                  />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item name={["filter", "doubleTest"]} label="Double Test">
                  <InputNumber type="number" />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "floatTest"]} label="Float Test">
                  <InputNumber type="number" />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "string"]} label="String">
                  <Input
                    suffix={
                      form.isFieldTouched(["filter", "string"]) ? (
                        <CloseCircleOutlined
                          onClick={() =>
                            form.resetFields([["filter", "string"]])
                          }
                        />
                      ) : (
                        <span />
                      )
                    }
                  />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "bool"]}
                  label="Bool"
                  valuePropName="checked"
                  initialValue={false}
                >
                  <Checkbox />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "bigInt"]} label="Big Int">
                  <InputNumber type="number" precision={0} stringMode />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "longTest"]} label="Long Test">
                  <InputNumber type="number" precision={0} stringMode />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "bigDecimal"]} label="Big Decimal">
                  <InputNumber type="number" stringMode />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "localDate"]} label="Local Date">
                  <DatePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "localDateTime"]}
                  label="Local Date Time"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "localTime"]} label="Local Time">
                  <TimePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item
                  name={["filter", "offsetDateTime"]}
                  label="Offset Date Time"
                >
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "offsetTime"]} label="Offset Time">
                  <TimePicker />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "dateTest"]} label="Date Test">
                  <DatePicker showTime={{ format: "HH:mm:ss" }} />
                </Form.Item>
              </Col>

              <Col span={6} style={{ display: showAll ? "block" : "none" }}>
                <Form.Item name={["filter", "url"]} label="Url">
                  <Input
                    type="url"
                    suffix={
                      form.isFieldTouched(["filter", "url"]) ? (
                        <CloseCircleOutlined
                          onClick={() => form.resetFields([["filter", "url"]])}
                        />
                      ) : (
                        <span />
                      )
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
          );
        }}
      </Form.Item>

      <Row justify="space-between">
        <Col>
          <Space>
            <Button type="primary" htmlType="submit">
              <FormattedMessage id="filters.apply" />
            </Button>
            <Button onClick={onResetFilters}>
              <FormattedMessage id="filters.reset" />
            </Button>
          </Space>
        </Col>

        <Col>
          <Button type="link" onClick={() => setShowAll(!showAll)}>
            <Space>
              <FormattedMessage
                id={showAll ? "filters.collapse" : "filters.showAll"}
              />
              {countHiddenInvalideFilters > 0 && (
                <Badge
                  style={{ backgroundColor: "#FF4D4F" }}
                  size="small"
                  count={countHiddenInvalideFilters}
                />
              )}
              {countHiddenInvalideFilters === 0 &&
                countHiddenTouchedFilters > 0 && (
                  <Badge
                    style={{ backgroundColor: "#1890ff" }}
                    size="small"
                    count={countHiddenTouchedFilters}
                  />
                )}
              {showAll ? <UpOutlined /> : <DownOutlined />}
            </Space>
          </Button>
        </Col>
      </Row>
    </Form>
  );
}

interface ItemCardsListProps {
  items?: ItemListType;
  loading?: boolean;
  error?: ApolloError;
}

/**
 * Collection of cards, each card representing an item
 */
function Cards({ items, loading, error }: ItemCardsListProps) {
  if (loading) {
    return <Spin />;
  }

  if (error) {
    return <RequestFailedError />;
  }

  if (items == null || items.length === 0) {
    return <Empty />;
  }

  return (
    <Space direction="vertical" className="card-space">
      {items.map(item => (
        <ItemCard item={item} key={item?.id} />
      ))}
    </Space>
  );
}

function ItemCard({ item }: { item: ItemType }) {
  // Get the action buttons that will be displayed in the card
  const cardActions: ReactNode[] = useCardActions(item);

  if (item == null) {
    return null;
  }

  return (
    <Card
      key={item.id}
      title={getScalarsTestEntityDisplayName(item)}
      actions={cardActions}
      className="narrow-layout"
    >
      <ValueWithLabel
        key="intTest"
        label="Int Test"
        value={item.intTest ?? undefined}
      />
      <ValueWithLabel
        key="intPrimitive"
        label="Int Primitive"
        value={item.intPrimitive ?? undefined}
      />
      <ValueWithLabel
        key="byteTest"
        label="Byte Test"
        value={item.byteTest ?? undefined}
      />
      <ValueWithLabel
        key="bytePrimitive"
        label="Byte Primitive"
        value={item.bytePrimitive ?? undefined}
      />
      <ValueWithLabel
        key="shortTest"
        label="Short Test"
        value={item.shortTest ?? undefined}
      />
      <ValueWithLabel
        key="shortPrimitive"
        label="Short Primitive"
        value={item.shortPrimitive ?? undefined}
      />
      <ValueWithLabel
        key="doubleTest"
        label="Double Test"
        value={item.doubleTest ?? undefined}
      />
      <ValueWithLabel
        key="doublePrimitive"
        label="Double Primitive"
        value={item.doublePrimitive ?? undefined}
      />
      <ValueWithLabel
        key="floatTest"
        label="Float Test"
        value={item.floatTest ?? undefined}
      />
      <ValueWithLabel
        key="floatPrimitive"
        label="Float Primitive"
        value={item.floatPrimitive ?? undefined}
      />
      <ValueWithLabel
        key="string"
        label="String"
        value={item.string ?? undefined}
      />
      <ValueWithLabel key="bool" label="Bool" value={item.bool ?? undefined} />
      <ValueWithLabel
        key="boolPrimitive"
        label="Bool Primitive"
        value={item.boolPrimitive ?? undefined}
      />
      <ValueWithLabel
        key="bigInt"
        label="Big Int"
        value={item.bigInt ?? undefined}
      />
      <ValueWithLabel
        key="longTest"
        label="Long Test"
        value={item.longTest ?? undefined}
      />
      <ValueWithLabel
        key="longPrimitive"
        label="Long Primitive"
        value={item.longPrimitive ?? undefined}
      />
      <ValueWithLabel
        key="bigDecimal"
        label="Big Decimal"
        value={item.bigDecimal ?? undefined}
      />
      <ValueWithLabel
        key="localDate"
        label="Local Date"
        value={item.localDate?.format("LL") ?? undefined}
      />
      <ValueWithLabel
        key="localDateTime"
        label="Local Date Time"
        value={item.localDateTime?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="localTime"
        label="Local Time"
        value={item.localTime?.format("LTS") ?? undefined}
      />
      <ValueWithLabel
        key="offsetDateTime"
        label="Offset Date Time"
        value={item.offsetDateTime?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="offsetTime"
        label="Offset Time"
        value={item.offsetTime?.format("LTS") ?? undefined}
      />
      <ValueWithLabel
        key="dateTest"
        label="Date Test"
        value={item.dateTest?.format("LLL") ?? undefined}
      />
      <ValueWithLabel
        key="url"
        label="Url"
        value={item.url ?? undefined}
        isUrl={true}
      />
    </Card>
  );
}

/**
 * Returns action buttons that will be displayed inside the card.
 */
function useCardActions(item: ItemType): ReactNode[] {
  const intl = useIntl();
  const navigate = useNavigate();
  const { showDeleteConfirm, deleting } = useDeleteConfirm(item?.id);

  return [
    <EditOutlined
      key="edit"
      title={intl.formatMessage({ id: "common.edit" })}
      onClick={() => {
        if (item?.id != null) {
          navigate(item.id);
        }
      }}
    />,
    deleting ? (
      <LoadingOutlined />
    ) : (
      <DeleteOutlined
        key="delete"
        title={intl.formatMessage({ id: "common.remove" })}
        onClick={showDeleteConfirm}
      />
    )
  ];
}

/**
 * Returns a confirmation dialog and invokes delete mutation upon confirmation
 * @param id id of the entity instance that should be deleted
 */
function useDeleteConfirm(id: string | null | undefined) {
  const intl = useIntl();

  const [runDeleteMutation, { loading }] = useMutation(
    DELETE_SCALARS_TEST_ENTITY
  );
  const deleteItem = useDeleteItem(id, runDeleteMutation, REFETCH_QUERIES);

  // Callback that deletes the item
  function handleDeleteItem() {
    deleteItem()
      .then(({ errors }: FetchResult) => {
        if (errors == null || errors.length === 0) {
          return handleDeleteSuccess();
        }
        return handleDeleteGraphQLError(errors);
      })
      .catch(handleDeleteNetworkError);
  }

  // Function that is executed when mutation is successful
  function handleDeleteSuccess() {
    return message.success(
      intl.formatMessage({ id: "EntityDetailsScreen.deletedSuccessfully" })
    );
  }

  // Function that is executed when mutation results in a GraphQL error
  function handleDeleteGraphQLError(
    errors: ReadonlyArray<GraphQLError> | undefined
  ) {
    console.error(errors);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  // Function that is executed when mutation results in a network error (such as 4xx or 5xx)
  function handleDeleteNetworkError(error: Error | ApolloError) {
    console.error(error);
    return message.error(intl.formatMessage({ id: "common.requestFailed" }));
  }

  return {
    showDeleteConfirm: () =>
      Modal.confirm({
        content: intl.formatMessage({
          id: "EntityListScreen.deleteConfirmation"
        }),
        okText: intl.formatMessage({ id: "common.ok" }),
        cancelText: intl.formatMessage({ id: "common.cancel" }),
        onOk: handleDeleteItem
      }),
    deleting: loading
  };
}

function stateToSearchParams(
  state: QueryVariablesType
): Record<string, string> {
  const { page, sort, ...filter } = state;
  const params: Record<string, string> = {};

  if (page != null) {
    params.pageNumber = String(page.number + 1);
    params.pageSize = String(page.size);
  }

  if (sort != null && !Array.isArray(sort)) {
    params.sortProperty = String(sort.property);
    params.sortDirection = String(sort.direction);
  }

  if (filter != null && Object.keys(filter).length > 0) {
    params.filter = JSON.stringify(filter);
  }

  return params;
}

function searchParamsToState(
  searchParams: URLSearchParams
): QueryVariablesType {
  let state: QueryVariablesType = {};
  const {
    pageNumber,
    pageSize,
    sortProperty,
    sortDirection,
    filter
  } = Object.fromEntries(searchParams.entries());

  if (pageNumber != null && pageSize != null) {
    state.page = {
      number: Number(pageNumber) - 1,
      size: Number(pageSize)
    };
  } else {
    state.page = {
      number: 0,
      size: DEFAULT_PAGE_SIZE
    };
  }

  if (sortProperty != null && sortDirection != null) {
    state.sort = {
      direction: sortDirection as SortDirection,
      property: sortProperty as ScalarsTestEntityOrderByProperty
    };
  }

  if (filter != null) {
    state = {
      ...state,
      ...JSON.parse(decodeURIComponent(filter))
    };
  }

  return state;
}

function extractFilterParams(state: QueryVariablesType) {
  const { page: _page, sort: _sort, ...filter } = state;
  return filter;
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<
  typeof SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE
>;
/**
 * Type of variables used to filter the items list
 */
type QueryVariablesType = VariablesOf<
  typeof SCALARS_TEST_ENTITY_LIST_WITH_FILTER_SORT_PAGE
>;
/**
 * Type of the items list
 */
type ItemListType = Exclude<
  QueryResultType["scalarsTestEntityList"],
  null | undefined
>["content"];
/**
 * Type of a single item
 */
type ItemType = Exclude<ItemListType, null | undefined>[0];
